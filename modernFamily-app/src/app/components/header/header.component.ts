import { Component, OnInit } from '@angular/core';
import { Header } from './models/header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public header: Header;
  constructor() {
    this.header = {
      icon: {
        img: '/assets/images/modern-family-logo.png',
        name: 'modern-family-logo'
      },
      links: ['Home', 'Personajes', 'About'],
    };
   }

  ngOnInit(): void {
  }

}
