export interface Home {
    content: Content;
    gallery: Gallery;
    articles: Articles;
}

export interface Content {
    img: Image;
}

export interface Gallery {
    img: Array<Image>;
}

export interface Articles {
    article: Array<Article>;
}

export interface Image {
    img: string;
    name: string;
}

export interface Article {
    title: string;
    subtitle: string;
    img: Image;
    content: string;
}
