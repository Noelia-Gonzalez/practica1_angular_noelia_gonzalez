import { Component, Input, OnInit } from '@angular/core';
import { Gallery, Image } from '../models/home';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
 @Input() gallery!: Gallery;
  constructor() { }

  ngOnInit(): void {
  }

}
