import { Component, Input, OnInit } from '@angular/core';
import { Content } from '../models/home';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  @Input() content!: Content;
  public flag: boolean = false;
  public state = "www.netflix.com";
  constructor() {}

  ngOnInit(): void {}

  onButtonClick() : void {
    this.flag = !this.flag;
  }
}
