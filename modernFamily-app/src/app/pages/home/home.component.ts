import { Home } from './models/home';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public home: Home;

  constructor() {
    this.home = {
      content: {
        img: {
          img: '/assets/images/modern-family-content.jpg',
          name: 'moder-family-content',
        },
      },
      gallery: {
        img: [
        {
          img: '/assets/images/gallery1.jpg',
          name: 'Family 1',
        },
        {
          img: '/assets/images/gallery2.jpg',
          name: 'Family 2',
        },
        {
          img: '/assets/images/gallery3.jpg',
          name: 'Family 3',
        },
      ],
      },
      articles: {
        article: [
          {
          title: 'Season',
          subtitle: '1',
          img: {
            img: '/assets/images/article1.jpg',
            name: 'icon-season-1',
          },
          content: 'La primera temporada de la serie de televisión de comedia, Modern Family se estrenó el 23 de septiembre de 2009 y terminó el 19 de mayo de 2010 en la American Broadcasting Company en los Estados Unidos',
        },
        {
          title: 'Season',
          subtitle: '2',
          img: {
            img: '/assets/images/article2.jpg',
            name: 'icon-season-2',
          },
          content: 'La segunda temporada de la serie de televisión de comedia, Modern Family se estrenó el 22 de septiembre de 2010 y terminó el 25 de mayo de 2011 en la American Broadcasting Company en los Estados Unidos',
        },
        {
          title: 'Season',
          subtitle: '3',
          img: {
            img: '/assets/images/article3.jpg',
            name: 'icon-season-3',
          },
          content: 'La tercera temporada de la serie de televisión de comedia, Modern Family se estrenó el 21 de septiembre de 2011 y terminó el 23 de mayo de 2012 en la American Broadcasting Company en los Estados Unidos',
        },
        {
          title: 'Season',
          subtitle: '4',
          img: {
            img: '/assets/images/article4.jpg',
            name: 'icon-season-4',
          },
          content: 'La cuarta temporada de la serie de televisión de comedia, Modern Family se estrenó el 26 de septiembre de 2012 y terminó el 22 de mayo de 2013 en la American Broadcasting Company en los Estados Unidos',
        },
        ],
      },
    };
   }

  ngOnInit(): void {
  }

}
